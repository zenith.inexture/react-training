import React from "react";
import "./Custom.css";

//shouldComponentUpdate - re-render when it's props changed.
//pure component is work on same shouldComponentUpdate

class Pure extends React.PureComponent {
  render() {
    return <h3 className="App">i'm pure component</h3>;
  }
}

export default Pure;
