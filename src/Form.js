import { useState } from "react";

function Form() {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [file, setFile] = useState();
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(username, password, file);
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input
            type="text"
            onChange={(e) => setUserName(e.target.value)}
            value={username}
          ></input>
        </label>
        <label>
          Password:
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          ></input>
        </label>
        {/* in react file always an uncontrolled element bcz of file value set by use but not programmatically.  */}
        <label>
          <input
            type="file"
            onChange={(e) => setFile(e.target.files[0].name)}
            //files[0] returns full object with details of file
          ></input>
        </label>
        <button type="submit">Submit</button>
      </form>
    </>
  );
}

export default Form;
