import React from "react";
import "./Custom.css";

//class components
class Class_compo extends React.Component {
  render() {
    return <h3 className="App">i'm class componet</h3>;
  }
}

export default Class_compo;

//Composing Components - we import component in another component and then use it
//Extracting components - we use componet after importing components
