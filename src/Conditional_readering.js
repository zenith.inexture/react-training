//conditional return with function component
//we can use class compo. with this keyword
function Condition_check(props) {
  //this is if/else approch
  if (props % 3 === 0) {
    return "YES";
  } else {
    return "NO";
  }

  //element variable
  // let message;
  // if (props % 3 === 0) {
  //   message = "YES";
  // } else {
  //   message = "NO";
  // }
  // return message;

  //ternory operator
  // return props % 3 === 0 ? "YES" : "NO";

  //short circuit syntax - but this syntac doe't return in else condition
  // return props % 3 === 0 && "YES";
}

export default Condition_check;
