import "./Custom.css";

//function component
//function component name must be in camel casing
function FunctionCompo(props) {
  return (
    <>
      <h3 className="App">i'm function component</h3>
      {props.children}
    </>
  );
}

export default FunctionCompo;
