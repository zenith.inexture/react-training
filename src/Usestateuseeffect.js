import React, { useEffect, useState } from "react";

//state in class component
// class Timer extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { name: "zenith" };
//   }
//   componentDidMount() {
//     this.setState({ name: "poriya" });
//   }
//   render() {
//     return <h3 className="App">Hello {this.state.name}</h3>;
//   }
// }

// export default Timer;

//usestate and useeffect in function component
function Usestateuseeffect() {
  //The first element is the current value of the state, and the second element is a state setter function
  const [name, setCount] = useState();
  const newname = "zenith";
  useEffect(() => {
    setCount(newname);
  }, [newname]);
  return <h3 className="App">Hello {name}</h3>;
}

export default Usestateuseeffect;
