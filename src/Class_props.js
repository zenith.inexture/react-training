import React from "react";
import "./Custom.css";

// React Props - props stands for properties.
// class Props extends React.Component {
//   render() {
//     return <h3 className="App">{this.props.name}</h3>;
//   }
// }

//props pass in function componet
//we can't change props(props are read only)
function Props(props) {
  return <h3 className="App">only one componet - {props.name}</h3>;
}

//more then one props pass or object passing in porps
// class Comment extends React.Component {
//   render() {
//     return (
//       <>
//         <h2 className="App">{this.props.text}</h2>
//         <h4 className="App">{this.props.data}</h4>
//       </>
//     );
//   }
// }

//more then one componet in function componet
function Comment(props) {
  return (
    <>
      <h3 className="App">first props - {props.data}</h3>
      <h3 className="App">second props - {props.text}</h3>
    </>
  );
}

export default Props;
export { Comment };
