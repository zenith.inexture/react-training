import React, { Component } from "react";
import FunctionMemoChild from "./FunctionMemoChild";

export class ClassMemoPerent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "zenith",
    };
  }
  //in set interval we can change the state in 2s interval
  componentDidMount() {
    setInterval(() => {
      this.setState({
        name: "zenith",
      });
    }, 2000);
  }
  render() {
    console.log("this is component did update method in class component");
    return <FunctionMemoChild name={this.state.name} />;
  }
}

export default ClassMemoPerent;
