import React from "react";

function FunctionMemoChild(props) {
  console.log("funcitonal memo component");
  return <div>{props.name}</div>;
}
//with the help of react memo the component no render if there no any change in props or state
export default React.memo(FunctionMemoChild);
