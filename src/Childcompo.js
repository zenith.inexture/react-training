import React from "react";

function Childcompo(props) {
  //props hellomethod is function is provided by the perent component
  return (
    <button onClick={() => props.hellomethod("hello guys i'm child")}>
      Click here to show magic in console
    </button>
  );
}

export default Childcompo;
