import React from "react";
import "./Custom.css";

function List(props) {
  return (
    <>
      {props.numbers.map((item) => (
        <span key={item}>{item}</span>
      ))}
    </>
  );
}

export default List;
