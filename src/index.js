import React from "react";
import ReactDOM from "react-dom";
import App from "./Hello_world";
import Car from "./Class_compo";
import FunctionCompo from "./Function_compo";
import Pure from "./Pure_compo";
import Props from "./Class_props";
import { Comment } from "./Class_props";
import Clock from "./Clock";
import LifeCycle from "./Lifecycle";
import Event from "./Event_hand";
import State from "./Compo_state";
import List from "./Lists_keys";
import Usestateuseeffect from "./Usestateuseeffect";
import Form from "./Form";
import Diffprops from "./Diffprops";
import Multistate from "./Multistate";
import ChidtoPerentprops from "./ChidtoPerentprops";
import ClassMemoPerent from "./ClassMemoPerent";
import ClassRefPerent from "./ClassRefPerent";

//Rendering a Component
//hello world rendering
ReactDOM.render(<App />, document.getElementById("root1"));

//class component
ReactDOM.render(<Car />, document.getElementById("root2"));

//function componet - name of functional component is always in camelcasing name
//like this we can pass children in any conponent and fetch this component with props.children
ReactDOM.render(
  <FunctionCompo>
    <p className="App">this is children in function component</p>
  </FunctionCompo>,
  document.getElementById("root3")
);

//pure component
ReactDOM.render(<Pure />, document.getElementById("root4"));

//props in
ReactDOM.render(
  <Props name="zenith poriya" />,
  document.getElementById("root5")
);

//pass more then one value as props
const comment = {
  data: 123,
  text: "More then one value in props",
};
ReactDOM.render(
  <Comment data={comment.data} text={comment.text} />,
  document.getElementById("root6")
);

// Clock
setInterval(function () {
  ReactDOM.render(<Clock />, document.getElementById("root7"));
}, 1000);

//Clock using LifeCycle method
ReactDOM.render(<LifeCycle />, document.getElementById("root8"));

//Event handling
ReactDOM.render(<Event />, document.getElementById("root9"));

//Component state
ReactDOM.render(<State />, document.getElementById("root10"));

const numbers = [1, 2, 3, 4, 5];
ReactDOM.render(<List numbers={numbers} />, document.getElementById("root11"));

//Timer with useEffect and useState
ReactDOM.render(<Usestateuseeffect />, document.getElementById("root12"));

ReactDOM.render(<Form />, document.getElementById("root13"));

ReactDOM.render(
  <Diffprops name={comment} />,
  document.getElementById("root14")
);
ReactDOM.render(
  <Diffprops name={numbers} />,
  document.getElementById("root15")
);

ReactDOM.render(<Multistate />, document.getElementById("root16"));

ReactDOM.render(<ChidtoPerentprops />, document.getElementById("root17"));

ReactDOM.render(<ClassMemoPerent />, document.getElementById("root18"));

ReactDOM.render(<ClassRefPerent />, document.getElementById("root19"));
