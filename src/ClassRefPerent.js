import React, { Component } from "react";
import { createRef } from "react/cjs/react.production.min";
import ClassRefChild from "./ClassRefChild";

export class ClassRefPerent extends Component {
  constructor(props) {
    super(props);

    this.compoRef = createRef();
  }

  buttonFocus = () => {
    this.compoRef.current.childFocusMethod();
  };
  render() {
    return (
      <>
        {/* this is the method to pass the ref in class component and foucs the element with the method of child component */}
        <ClassRefChild ref={this.compoRef} />
        <button onClick={this.buttonFocus}>Class ref demo click me</button>
      </>
    );
  }
}

export default ClassRefPerent;
