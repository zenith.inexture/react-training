import React from "react";
import Childcompo from "./Childcompo";

function ChidtoPerentprops() {
  //this is function that we can pass into our props
  function helloporps(props) {
    console.log(props);
  }
  //with this we can pass method to props and access this method in child and pass props in child to perent
  return <Childcompo hellomethod={helloporps} />;
}

export default ChidtoPerentprops;
