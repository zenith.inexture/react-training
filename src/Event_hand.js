import React from "react";

class Event extends React.Component {
  constructor(props) {
    super(props);
    this.state = { display: "Show", isToggleOn: false };

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  //event name must be in camelcase
  handleClick() {
    this.setState((prevState) => ({
      isToggleOn: !prevState.isToggleOn,
    }));
    // this.setState(
    //   //we can set the state based on condition also
    //   this.state.isToggleOn ? { display: "Show" } : { display: "Hide" }
    // );
  }

  render() {
    return (
      <>
        <button onClick={this.handleClick} className="btn">
          {this.state.isToggleOn ? "Hide" : "Show"}
        </button>
        {this.state.isToggleOn ? <h1>Hello world!</h1> : null}
      </>
    );
  }
}

export default Event;
