import React from "react";
import "./Custom.css";

class LifeCycle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date(), count: 0 };
  }

  componentDidMount() {
    //these methods are called LifeCycle mehtods
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timerID);
    this.setState({ count: 5 }, () => {
      console.log(this.state.count);
    });
  };

  tick() {
    this.setState({
      //we can change the state like this or with previous state also we can't do directly
      date: new Date(),
    });
  }

  render() {
    return (
      <div>
        <h2 className="App">lifeCycle Methods</h2>
        <span>{this.state.count}</span>
        <h4 className="App">It is {this.state.date.toLocaleTimeString()}.</h4>
        <button onClick={this.componentWillUnmount}>Stop</button>
        {/* also we can pass lifecycle methods in events like onclick and onhover*/}
        {/* we can bind function in inline react with {() => function_name()} */}
      </div>
    );
  }
}

export default LifeCycle;
