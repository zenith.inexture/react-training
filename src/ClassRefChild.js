import React, { Component } from "react";
import { createRef } from "react/cjs/react.production.min";

export class ClassRefChild extends Component {
  constructor(props) {
    super(props);

    this.newRef2 = createRef();
  }

  childFocusMethod() {
    this.newRef2.current.focus();
  }

  render() {
    return <input ref={this.newRef2}></input>;
  }
}

export default ClassRefChild;
