import React from "react";

class Multistate extends React.Component {
  constructor() {
    super();
    this.state = {
      user: [
        {
          name: "Akshat Mehta",
          EnrollmentNumber: "180320107045",
        },
        {
          name: "Jinay Shah",
          EnrollmentNumber: "180320108471",
        },
      ],
    };
  }

  changeProps = () => {
    this.setState({
      user: [
        {
          name: "Keval Mehta",
          EnrollmentNumber: "200010004000",
        },
        {
          name: "Rutvik Shah",
          EnrollmentNumber: "110004000741",
        },
      ],
    });
  };

  render() {
    return (
      <>
        {this.state.user.map((i) => {
          return (
            <span key={i.name}>
              {i.name} {i.EnrollmentNumber}
              <br></br>
            </span>
          );
        })}
        <button onClick={this.changeProps}>
          Update Props in Class Component
        </button>
      </>
    );
  }
}
export default Multistate;
