import React from "react";
import "./Custom.css";

class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {data: "Clock"};
    }
  
    render() {
      return (
        <>
          <h2 className="App">{this.state.data}</h2>
          <h4 className="App">{new Date().toLocaleTimeString()}.</h4>
        </>
      );
    }
  }
export default Clock;
