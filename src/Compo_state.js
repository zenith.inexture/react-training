import React from "react";
import Condition_check from "./Conditional_readering";

class State extends React.Component {
  constructor(props) {
    super(props);
    //we can write state outside the constructor also
    this.state = { count: 0 };

    //this is real binding syntax.
    this.resetState = this.resetState.bind(this);
  }

  //we can bind function in this way also but this is experimental way
  updateState = () => {
    // Changing state
    this.setState(
      (prevState) => ({
        count: prevState.count + 1,
      }),
      () => {
        //if we used callbacke function in setstate then we can fetch the updated value from state
        console.log("value of count is : ", this.state.count);
      }
    );
  };
  updateStateFiveTimes = () => {
    //if we used method like this and that method contain only setstate then we have to use previous state in that function for updating the state
    this.updateState();
    this.updateState();
    this.updateState();
    this.updateState();
    this.updateState();
  };
  resetState() {
    // Changing state
    this.setState({ count: 0 });
  }

  render() {
    return (
      <>
        <h3>{this.state.count}</h3>
        <h4>Value is divisible by 3 or Not</h4>
        <h4>{Condition_check(this.state.count)}</h4>
        <button onClick={this.updateState} className="btn">
          Let's Go!
        </button>
        <button onClick={this.updateStateFiveTimes} className="btn">
          Add five on this value
        </button>
        <button onClick={this.resetState} className="btn">
          Reset!
        </button>
      </>
    );
  }
}

export default State;
