import "./Custom.css";

//Function Component - created using a Function component
function App() {
  return (
    <div className="App">
      <h1>Hello world!!</h1>
    </div>
  );
}

export default App;
